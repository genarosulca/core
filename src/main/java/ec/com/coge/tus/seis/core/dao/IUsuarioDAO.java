package ec.com.coge.tus.seis.core.dao;

import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import ec.com.coge.tus.seis.core.dto.UsuarioDTO;

@Transactional
public interface IUsuarioDAO {

	Collection<UsuarioDTO> obtenerUsuarios();
}
