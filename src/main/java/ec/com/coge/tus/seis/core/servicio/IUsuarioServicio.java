package ec.com.coge.tus.seis.core.servicio;

import java.util.Collection;

import ec.com.coge.tus.seis.core.dto.UsuarioDTO;

public interface IUsuarioServicio {

	Collection<UsuarioDTO> obtenerUsuarios();
}
