package ec.com.coge.tus.seis.core.gestor;

import java.util.Collection;

import ec.com.coge.tus.seis.core.dto.UsuarioDTO;

public interface IUsuarioGestor {

	Collection<UsuarioDTO> obtenerUsuarios();
}
