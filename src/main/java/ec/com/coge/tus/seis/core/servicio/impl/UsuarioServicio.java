package ec.com.coge.tus.seis.core.servicio.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.coge.tus.seis.core.dto.UsuarioDTO;
import ec.com.coge.tus.seis.core.gestor.IUsuarioGestor;
import ec.com.coge.tus.seis.core.servicio.IUsuarioServicio;

@Service
public class UsuarioServicio implements IUsuarioServicio{
	
	@Autowired
	private IUsuarioGestor usuarioGestor;

	@Override
	public Collection<UsuarioDTO> obtenerUsuarios() {
		return usuarioGestor.obtenerUsuarios();
	}

}
