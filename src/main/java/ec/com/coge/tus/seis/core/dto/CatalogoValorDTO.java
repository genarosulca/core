package ec.com.coge.tus.seis.core.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CATALOGOVALOR")
public class CatalogoValorDTO {

	@Id
	@Column(name = "IDCATALOGOVALOR")
	private Integer idCatalogoValor;
	
	@Column(name = "IDCATALOGOTIPO")
	private Integer idCatalogoTipo;
	
	@Column(name = "DESCRIPCIONCATALOGOVALOR")
	private String descripcionCatalogoValor;
	
//	@JoinColumn(name="userlastname_fk", referencedColumnName="lastName")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDCATALOGOVALOR", nullable = false, insertable= false, updatable = false)
	private CatalogoTipoDTO catalogoTipo;
}
