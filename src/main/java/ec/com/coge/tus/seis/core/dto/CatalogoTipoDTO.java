package ec.com.coge.tus.seis.core.dto;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CATALOGOTIPO")
public class CatalogoTipoDTO {

	@Id
	@Column(name = "IDCATALOGOTIPO")
	private Integer idCatalogoTipo;
	
	@Column(name = "DESCRIPCIONCATALOGOTIPO")
	private String descripcionCatalogoTipo;
	
	@Column(name = "FILANO")
	private String filaNo;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "catalogoTipo")
	Collection<CatalogoValorDTO> catalogoValorHijo;

	public Integer getIdCatalogoTipo() {
		return idCatalogoTipo;
	}

	public void setIdCatalogoTipo(Integer idCatalogoTipo) {
		this.idCatalogoTipo = idCatalogoTipo;
	}

	public String getDescripcionCatalogoTipo() {
		return descripcionCatalogoTipo;
	}

	public void setDescripcionCatalogoTipo(String descripcionCatalogoTipo) {
		this.descripcionCatalogoTipo = descripcionCatalogoTipo;
	}

	public String getFilaNo() {
		return filaNo;
	}

	public void setFilaNo(String filaNo) {
		this.filaNo = filaNo;
	}

	public Collection<CatalogoValorDTO> getCatalogoValorHijo() {
		return catalogoValorHijo;
	}

	public void setCatalogoValorHijo(Collection<CatalogoValorDTO> catalogoValorHijo) {
		this.catalogoValorHijo = catalogoValorHijo;
	}
	
	
}
