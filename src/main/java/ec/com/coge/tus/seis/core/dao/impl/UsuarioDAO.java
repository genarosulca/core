package ec.com.coge.tus.seis.core.dao.impl;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.com.coge.tus.seis.core.dao.IUsuarioDAO;
import ec.com.coge.tus.seis.core.dto.UsuarioDTO;

@Component
public class UsuarioDAO implements IUsuarioDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public Collection<UsuarioDTO> obtenerUsuarios() {
		Criteria criteria = sessionFactory.openSession().createCriteria(UsuarioDTO.class);
		return (Collection<UsuarioDTO>) criteria.list();
	}
}
