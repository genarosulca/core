package ec.com.coge.tus.seis.core.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "USUARIO")
public class UsuarioDTO implements Serializable {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "IDUSUARIO")
	private Integer idUsuario;
	
	@Column(name = "NUMERODOCUMENTO")
	private String numeroDocumento;
	
	@Column(name = "PRIMERNOMBRE")
	private String primerNombre;
	
	@Column(name = "SEGUNDONOMBRE")
	private String segundoNombre;
	
	@Column(name = "PRIMERAPELLIDO")
	private String primerApellido;
	
	@Column(name = "SEGUNDOAPELLIDO")
	private String segundoApellido;
	
	@Column(name = "CANTIDADPUNTOSFIDELIDAD")
	private Integer puntosFidelidad;
	
	@Column(name = "GENERARECOMPENSA")
	private Boolean generaRecompensa;
	
	@Column(name = "USERNAME")
	private String username;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "CONTACTOEQUIPO")
	private Boolean contactoEquipo;
	
	@Column(name = "ESTADO")
	private String estado;
	
	@Column(name = "FECHACREACION")
	private Date fechaCreacion;
	
	@Column(name = "FECHAMODIFICACION")
	private Date fechaModificacion;
	
	
	@Column(name = "CATALOGOVALORTIPODOCUMENTO")
	private Integer catalogoValorTipoDocumento;
	
	@Column(name = "CATALOGOTIPOTIPODOCUMENTO")
	private Integer catalogoTipoTipoDocumento;
	
	@Column(name = "CATALOGOTIPOGENEROUSUARIO")
	private Integer catalogoTipoGeneroUsuario;
	
	@Column(name = "CATALOGOVALORGENEROUSUARIO")
	private Integer catalogoValorGeneroUsuario;
	
	@Column(name = "CATALOGOTIPOTIPOUSUARIO")
	private Integer catalogoTipoTipoUsuario;
	
	@Column(name = "CATALOGOVALORTIPOUSUARIO")
	private Integer catalogoValorTipoUsuario;
	
	

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public Integer getPuntosFidelidad() {
		return puntosFidelidad;
	}

	public void setPuntosFidelidad(Integer puntosFidelidad) {
		this.puntosFidelidad = puntosFidelidad;
	}

	public Boolean getGeneraRecompensa() {
		return generaRecompensa;
	}

	public void setGeneraRecompensa(Boolean generaRecompensa) {
		this.generaRecompensa = generaRecompensa;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getContactoEquipo() {
		return contactoEquipo;
	}

	public void setContactoEquipo(Boolean contactoEquipo) {
		this.contactoEquipo = contactoEquipo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Integer getCatalogoValorTipoDocumento() {
		return catalogoValorTipoDocumento;
	}

	public void setCatalogoValorTipoDocumento(Integer catalogoValorTipoDocumento) {
		this.catalogoValorTipoDocumento = catalogoValorTipoDocumento;
	}

	public Integer getCatalogoTipoTipoDocumento() {
		return catalogoTipoTipoDocumento;
	}

	public void setCatalogoTipoTipoDocumento(Integer catalogoTipoTipoDocumento) {
		this.catalogoTipoTipoDocumento = catalogoTipoTipoDocumento;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getCatalogoTipoGeneroUsuario() {
		return catalogoTipoGeneroUsuario;
	}

	public void setCatalogoTipoGeneroUsuario(Integer catalogoTipoGeneroUsuario) {
		this.catalogoTipoGeneroUsuario = catalogoTipoGeneroUsuario;
	}

	public Integer getCatalogoValorGeneroUsuario() {
		return catalogoValorGeneroUsuario;
	}

	public void setCatalogoValorGeneroUsuario(Integer catalogoValorGeneroUsuario) {
		this.catalogoValorGeneroUsuario = catalogoValorGeneroUsuario;
	}

	public Integer getCatalogoTipoTipoUsuario() {
		return catalogoTipoTipoUsuario;
	}

	public void setCatalogoTipoTipoUsuario(Integer catalogoTipoTipoUsuario) {
		this.catalogoTipoTipoUsuario = catalogoTipoTipoUsuario;
	}

	public Integer getCatalogoValorTipoUsuario() {
		return catalogoValorTipoUsuario;
	}

	public void setCatalogoValorTipoUsuario(Integer catalogoValorTipoUsuario) {
		this.catalogoValorTipoUsuario = catalogoValorTipoUsuario;
	}
	
//	@OneToMany
//    @JoinColumns(
//    {
//        @JoinColumn(updatable=false,insertable=false, name="catalogoValorTipoDocumento", referencedColumnName="idCatalogoValor"),
//        @JoinColumn(updatable=false,insertable=false, name="catalogoTipoTipoDocumento", referencedColumnName="idCatalogoTipo"),
//    })
//    private CatalogoValor catalogoTipoDocumento;
	
	
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "catalogoTipo")
	
	
	
//	@Column(name = "IDEQUIPO")
//	private Integer idEquipo;
	
	
}
