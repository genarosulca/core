package ec.com.coge.tus.seis.core.gestor.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.com.coge.tus.seis.core.dao.IUsuarioDAO;
import ec.com.coge.tus.seis.core.dto.UsuarioDTO;
import ec.com.coge.tus.seis.core.gestor.IUsuarioGestor;

@Component
public class UsuarioGestor implements IUsuarioGestor{

	@Autowired
	private IUsuarioDAO usuarioDao;

	@Override
	public Collection<UsuarioDTO> obtenerUsuarios() {
		return usuarioDao.obtenerUsuarios();
	}
}
