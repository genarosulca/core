package ec.com.coge.tus.seis.core.dto;

import java.util.Date;

import javax.persistence.Column;

public class AuditoriaModelDTO {

	@Column(name = "FECHACREACION")
	private Date fechaCreacion;
	
	@Column(name = "FECHAMODIFICACION")
	private Date fechaModificacion;
	
	@Column(name = "USUARIOCREACION")
	private Integer usuarioCreacion;
	
	@Column(name = "USUARIOMODIFICACION")
	private Integer usuarioModificacion;
	
	@Column(name = "ESTADO")
	private String estado;
	
}
